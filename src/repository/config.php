<?php

class PDO_singleton {
    private static $instance;
    public static function getInstance() {
        if (!isset(self::$instance)) {
            $host = '172.22.0.10';
            $db = 'db';
            $port = '5432';
            $user = 'docker';
            $pass = 'docker';
            $charset = 'utf8mb4';
            
            $dsn = "pgsql:host=$host;port=$port;dbname=$db";
            $options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            
            try {
                self::$instance = new PDO($dsn, $user, $pass, $options);
            } catch (\PDOException $e) {
                throw new \PDOException($e->getMessage(), (int)$e->getCode());
            }
        }

        return self::$instance;
    }

}