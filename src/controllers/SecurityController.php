<?php

require_once "src/repository/config.php";
require_once "src/controllers/AppController.php";

class SecurityController extends AppController {

    public function signup() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $email = $_POST['email'];
            $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            
            $stmt = PDO_singleton::getInstance()->prepare("INSERT INTO users (email, password, name, surname) VALUES (?, ?, ?, ?)");
            if ($stmt->execute([$email, $password, $name, $surname])) {
                header("Location: login");
            } else {
                echo "Error: Could not register user.";
            }
        }
    }

    public function signin() {

        session_start();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $username = $_POST['email'];
            $password = $_POST['password'];
            
            $stmt = PDO_singleton::getInstance()->prepare("SELECT * FROM users WHERE email = ?");
            $stmt->execute([$username]);
            $user = $stmt->fetch();
            
            if ($user && password_verify($password, $user['password'])) {
                $token = bin2hex(random_bytes(16)); // Generate a random token
                $stmt = PDO_singleton::getInstance()->prepare("UPDATE users SET token = ? WHERE id = ?");
                $stmt->execute([$token, $user['id']]);
                
                $_SESSION['user_id'] = $user['id'];
                $_SESSION['token'] = $token;
                $_SESSION["username"] = $user["name"]." ".$user["surname"];
                $_SESSION["email"] = $user["email"];

                
                header("Location: dashboard");
            } else {
                echo "Invalid username or password.";
            }
        }
    }

    public function signout() {
        // session_start();

        if (isset($_SESSION['user_id'])) {
            $stmt = PDO_singleton::getInstance()->prepare("UPDATE users SET token = NULL WHERE id = ?");
            $stmt->execute([$_SESSION['user_id']]);
        }

        session_destroy();
        echo "Logged out successfully.";
    }
}