<?php

class AppController {
    protected function render(string $template = null, bool $register = false) {

        if ($register) {
            include 'public/views/'.$template.'.php';
        } else {
            $template_path = 'public/views/'.$template.'.php';

            if (file_exists($template_path)) {
                $content_file = $template_path;
            }
            
            include 'public/views/template.php';
        }
    }
}