<?php

require_once "src/repository/config.php";
require_once "src/controllers/AppController.php";

class NotificationController extends AppController {
    public function delete_notification() {
        header('Content-Type: application/json');

        // Pobranie danych JSON z żądania POST
        $data = json_decode(file_get_contents('php://input'), true);

        // Sprawdzenie, czy dane zostały przesłane
        if (!empty($data)) {
            try {
                // Przygotowanie zapytania SQL do wstawienia danych
                $sql = "DELETE FROM notifications WHERE id = :id";
                
                // Przygotowanie i wykonanie zapytania
                $stmt = PDO_singleton::getInstance()->prepare($sql);
                $stmt->bindParam(':id', $data['notification_id'], PDO::PARAM_INT);
                if ($stmt->execute()) {
                    echo json_encode(["status"=> "success", "message"=> "Notification deleted!"]);
                } else {
                    echo json_encode(["status"=> "error", "message"=> "Cannot delete notification!"]);
                }
            } catch (PDOException $e) {
                echo json_encode(["status" => "error", "message" => "Database error: " . $e->getMessage()]);
            }
        } else {
            echo json_encode(["status" => "error", "message" => "No data received!"]);
        }
    }

    public function delete_all_notifications() {
        header('Content-Type: application/json');

        // Pobranie danych JSON z żądania POST
        $data = json_decode(file_get_contents('php://input'), true);

        // Sprawdzenie, czy dane zostały przesłane
        if (!empty($data)) {
            
            try {
                // Przygotowanie zapytania SQL do wstawienia danych
                $sql = "DELETE FROM notifications WHERE user_id = :id";
                // $json_data = json_encode($data['notification_id']);

                // Przygotowanie i wykonanie zapytania
                $stmt = PDO_singleton::getInstance()->prepare($sql);
                $stmt->bindParam(':id', $data['user_id'], PDO::PARAM_INT);
                if ($stmt->execute()) {
                    echo json_encode(["status"=> "success", "message"=> "Notifications deleted!"]);
                } else {
                    echo json_encode(["status"=> "error", "message"=> "Cannot delete notifications!"]);
                }
            } catch (PDOException $e) {
                echo json_encode(["status" => "error", "message" => "Database error: " . $e->getMessage()]);
            }
        } else {
            echo json_encode(["status" => "error", "message" => "No data received!"]);
        }
    }
}