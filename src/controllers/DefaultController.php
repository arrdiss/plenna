<?php

require_once "src/controllers/AppController.php";

class DefaultController extends AppController {
    // display index.html
    public function login() {
        $this->render(__FUNCTION__, true);
    }

    // display dashboard.html
    public function dashboard() {
        $this->render(__FUNCTION__);
    }

    public function analysis() {
        $this->render(__FUNCTION__);
    }

    public function notifications() {
        $this->render(__FUNCTION__);
    }

    public function create_route() {
        $this->render(__FUNCTION__);
    }

    public function search_routes() {
        $this->render(__FUNCTION__);
    }

    public function register() {
        $this->render(__FUNCTION__, true);
    }
}