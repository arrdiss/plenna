<?php

require_once "src/repository/config.php";
require_once "src/controllers/AppController.php";

class RoutesController extends AppController {
    public function save_route() {
        header('Content-Type: application/json');

        // Pobranie danych JSON z żądania POST
        $data = json_decode(file_get_contents('php://input'), true);

        // Sprawdzenie, czy dane zostały przesłane
        if (!empty($data)) {
            
            try {
                // Przygotowanie zapytania SQL do wstawienia danych
                $sql = "INSERT INTO routes (markers, name, author_id, counter) VALUES (:route_data, :name, :user_id, 0) RETURNING id";
                $json_data = json_encode($data['route']);

                $u_id = $_SESSION['user_id'];

                // Przygotowanie i wykonanie zapytania
                $stmt = PDO_singleton::getInstance()->prepare($sql);
                $stmt->bindParam(':name', $data['name'], PDO::PARAM_STR);
                $stmt->bindParam(':route_data', $json_data, PDO::PARAM_LOB);
                $stmt->bindParam(':user_id', $u_id, PDO::PARAM_INT);

                PDO_singleton::getInstance()->beginTransaction();
                if ($stmt->execute()) {
                    $lastInsertId = $stmt->fetchColumn();
                    $sql = 'INSERT INTO routes_users (user_id, route_id) VALUES (:user_id, :route_id)';
                    $stmt = PDO_singleton::getInstance()->prepare($sql);
                    $stmt->bindParam(':user_id', $u_id, PDO::PARAM_INT);
                    $stmt->bindParam(':route_id', $lastInsertId, PDO::PARAM_INT);
                    if ($stmt->execute()) {
                        PDO_singleton::getInstance()->commit();
                        echo json_encode(["status" => "success", "message" => "Route saved successfully!"]);
                    } else {
                        PDO_singleton::getInstance()->rollBack();
                        echo json_encode(["status"=> "error", "message"=> "Cannot connect route to user!"]);
                    }
                } else {
                    PDO_singleton::getInstance()->rollBack();
                    echo json_encode(["status"=> "error", "message"=> "Cannot save route!"]);
                }
            } catch (PDOException $e) {
                PDO_singleton::getInstance()->rollBack();
                echo json_encode(["status" => "error", "message" => "Database error: " . $e->getMessage()]);
            }
        } else {
            echo json_encode(["status" => "error", "message" => "No data received!"]);
        }
    }
    public function search_routes() {
        header('Content-Type: application/json');

        // Pobranie danych JSON z żądania POST
        $data = json_decode(file_get_contents("php://input"), true);

        // Sprawdzenie, czy dane zostały przesłane
        if (!empty($data['name'])) {
            $routeName = $data['name'];
            try {
                // Przygotowanie zapytania SQL do wyszukania danych
                $sql = "SELECT id, name FROM routes WHERE name ILIKE :name;";
                $stmt = PDO_singleton::getInstance()->prepare($sql);
                $searchTerm = '%' . $routeName . '%';
                $stmt->bindParam(':name', $searchTerm, PDO::PARAM_STR);
                $stmt->execute();

                // Pobranie wyników
                $routes = $stmt->fetchAll(PDO::FETCH_ASSOC);

                echo json_encode([
                    "status" => "success",
                    "routes" => $routes
                ]);
            } catch (PDOException $e) {
                echo json_encode([
                    "status" => "error",
                    "message" => "Database error: " . $e->getMessage()
                ]);
            }
        } else {
            echo json_encode([
                "status" => "error",
                "message" => "No search term provided!"
            ]);
        }
    }

    public function save_user_route() {
        header('Content-Type: application/json');

        // Pobranie danych JSON z żądania POST
        $data = json_decode(file_get_contents('php://input'), true);

        // Sprawdzenie, czy dane zostały przesłane
        if (!empty($data)) {
            
            try {
                // Przygotowanie zapytania SQL do wstawienia danych
                $sql = "INSERT INTO routes_users (route_id, user_id) VALUES (:route_id, :user_id)";

                $u_id = $_SESSION['user_id'];

                // Przygotowanie i wykonanie zapytania
                $stmt = PDO_singleton::getInstance()->prepare($sql);
                $stmt->bindParam(':route_id', $data['id'], PDO::PARAM_INT);
                $stmt->bindParam(':user_id', $u_id, PDO::PARAM_INT);

                if ($stmt->execute()) {
                    echo json_encode(["status" => "success", "message" => "Route saved successfully!"]);
                } else {
                    echo json_encode(["status"=> "error", "message"=> "Cannot save route!"]);
                }
            } catch (PDOException $e) {
                if ($e->getCode() == 23505)
                    echo json_encode(["status" => "error", "message" => "Database error: You already saved this route!"]);
                else
                    echo json_encode(["status" => "error", "message" => "Database error: " . $e->getMessage()]);
            }
        } else {
            echo json_encode(["status" => "error", "message" => "No data received!"]);
        }
    }
    public function show_route() {
        header('Content-Type: application/json');

        // Pobranie danych JSON z żądania POST
        $data = json_decode(file_get_contents("php://input"), true);

        // Sprawdzenie, czy dane zostały przesłane
        if (!empty($data['id'])) {
            $routeId= $data['id'];
            try {
                // Przygotowanie zapytania SQL do wyszukania danych
                $sql = "SELECT markers FROM routes WHERE id = :id;";
                $stmt = PDO_singleton::getInstance()->prepare($sql);
                $stmt->bindParam(':id', $routeId, PDO::PARAM_INT);
                $stmt->execute();

                // Pobranie wyników
                $markers = $stmt->fetch();
                // print_r($markers);

                echo json_encode([
                    "status" => "success",
                    "markers" => stream_get_contents($markers['markers'])
                ]);
            } catch (PDOException $e) {
                echo json_encode([
                    "status" => "error",
                    "message" => "Database error: " . $e->getMessage()
                ]);
            }
        } else {
            echo json_encode([
                "status" => "error",
                "message" => "No search term provided!"
            ]);
        }
    }
    public function delete_route() {
        header('Content-Type: application/json');

        // Pobranie danych JSON z żądania POST
        $data = json_decode(file_get_contents('php://input'), true);

        // Sprawdzenie, czy dane zostały przesłane
        if (!empty($data)) {
            
            try {
                // Przygotowanie zapytania SQL do wstawienia danych
                $sql = "UPDATE routes SET author_id = NULL WHERE id = :id AND author_id = :author_id";
                
                $u_id = $_SESSION['user_id'];

                // Przygotowanie i wykonanie zapytania
                $stmt = PDO_singleton::getInstance()->prepare($sql);
                $stmt->bindParam(':id', $data['id'], PDO::PARAM_INT);
                $stmt->bindParam(':author_id', $u_id, PDO::PARAM_INT);

                PDO_singleton::getInstance()->beginTransaction();
                $stmt->execute();

                $sql = "DELETE FROM routes_users WHERE route_id = :id AND user_id = :user_id";
                
                // Przygotowanie i wykonanie zapytania
                $stmt = PDO_singleton::getInstance()->prepare($sql);
                $stmt->bindParam(':id', $data['id'], PDO::PARAM_INT);
                $stmt->bindParam(':user_id', $u_id, PDO::PARAM_INT);

                if ($stmt->execute()) {
                    PDO_singleton::getInstance()->commit();
                    echo json_encode(["status" => "success", "message" => "Route successfully deleted!"]);
                } else {
                    PDO_singleton::getInstance()->rollBack();
                    echo json_encode(["status"=> "error", "message"=> "Cannot delete route!"]);
                }
            } catch (PDOException $e) {
                PDO_singleton::getInstance()->rollBack();
                echo json_encode(["status" => "error", "message" => "Database error: " . $e->getMessage()]);
            }
        } else {
            echo json_encode(["status" => "error", "message" => "No data received!"]);
        }
    }
    public function get_route_stats() {
        $user_id = $_SESSION['user_id'];
try {
    // Pobierz nazwy tras i liczniki dla tras autorstwa zalogowanego użytkownika
    $sql = "
        SELECT r.name, r.counter
        FROM routes r
        WHERE r.author_id = :user_id
    ";
    $stmt = PDO_singleton::getInstance()->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->execute();
    $routes = $stmt->fetchAll(PDO::FETCH_ASSOC);

    // Zwróć dane w formacie JSON
    echo json_encode($routes);

} catch (PDOException $e) {
    echo json_encode(['error' => 'Database error: ' . $e->getMessage()]);
}
    }
}