--
-- PostgreSQL database dump
--

-- Dumped from database version 16.3 (Debian 16.3-1.pgdg120+1)
-- Dumped by pg_dump version 16.2

-- Started on 2024-06-16 06:24:43 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: pg_database_owner
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO pg_database_owner;

--
-- TOC entry 3408 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: pg_database_owner
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 227 (class 1255 OID 16450)
-- Name: decrement_counter(); Type: FUNCTION; Schema: public; Owner: docker
--

CREATE FUNCTION public.decrement_counter() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Sprawdzenie, czy author_id jest różne od NULL
    IF (SELECT author_id FROM routes WHERE id = OLD.route_id) IS NOT NULL THEN
        -- Aktualizacja wartości counter w tabeli routes_statistics
        UPDATE routes
        SET counter = counter - 1
        WHERE id = OLD.route_id;
    END IF;
    
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.decrement_counter() OWNER TO docker;

--
-- TOC entry 225 (class 1255 OID 16445)
-- Name: delete_route_if_no_users(); Type: FUNCTION; Schema: public; Owner: docker
--

CREATE FUNCTION public.delete_route_if_no_users() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Sprawdź, czy istnieją jakiekolwiek powiązania dla danej trasy
    IF NOT EXISTS (SELECT 1 FROM routes_users WHERE route_id = OLD.route_id) THEN
        -- Usuń trasę, jeśli nie ma żadnych powiązań
        DELETE FROM routes WHERE id = OLD.route_id;
    END IF;

    RETURN OLD;
END;
$$;


ALTER FUNCTION public.delete_route_if_no_users() OWNER TO docker;

--
-- TOC entry 226 (class 1255 OID 16447)
-- Name: get_route_user_count(integer, integer); Type: FUNCTION; Schema: public; Owner: docker
--

CREATE FUNCTION public.get_route_user_count(route_id integer, author_id integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_count INTEGER;
BEGIN
    -- Zapytanie do zliczenia ilości wpisów dla danego route_id
    SELECT COUNT(*)
    INTO v_count
    FROM routes_users
    WHERE route_id = route_id;

    -- Zwróć wynik
    RETURN v_count;
END;
$$;


ALTER FUNCTION public.get_route_user_count(route_id integer, author_id integer) OWNER TO docker;

--
-- TOC entry 228 (class 1255 OID 16448)
-- Name: increment_counter(); Type: FUNCTION; Schema: public; Owner: docker
--

CREATE FUNCTION public.increment_counter() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Sprawdzenie, czy author_id jest różne od NULL
    IF (SELECT author_id FROM routes WHERE id = NEW.route_id) IS NOT NULL THEN
        -- Aktualizacja wartości counter w tabeli routes_statistics
        UPDATE routes
        SET counter = counter + 1
        WHERE id = NEW.route_id;
    END IF;
    
    RETURN NEW;
END;
$$;


ALTER FUNCTION public.increment_counter() OWNER TO docker;

--
-- TOC entry 229 (class 1255 OID 16439)
-- Name: notify_route_saved(); Type: FUNCTION; Schema: public; Owner: docker
--

CREATE FUNCTION public.notify_route_saved() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    v_route_name TEXT;
    v_author_id INTEGER;
    v_notification_message TEXT;
BEGIN
    -- Pobierz nazwę trasy na podstawie route_id
    SELECT name, author_id INTO v_route_name, v_author_id
    FROM routes
    WHERE id = NEW.route_id;

    -- Jeżeli autor trasy jest NULL, nie wykonuj żadnych akcji
    IF v_author_id IS NULL THEN
        RETURN NEW;
    END IF;

    -- Utwórz wiadomość powiadomienia
    v_notification_message := 'Somebody saved your route "' || v_route_name || '"!';

    -- Wstaw nowy wpis do tabeli notification
    INSERT INTO notifications (user_id, message)
    VALUES (v_author_id, v_notification_message);

    RETURN NEW;
END;
$$;


ALTER FUNCTION public.notify_route_saved() OWNER TO docker;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 220 (class 1259 OID 16413)
-- Name: notifications; Type: TABLE; Schema: public; Owner: docker
--

CREATE TABLE public.notifications (
    id integer NOT NULL,
    user_id integer NOT NULL,
    message character varying(255) NOT NULL
);


ALTER TABLE public.notifications OWNER TO docker;

--
-- TOC entry 219 (class 1259 OID 16412)
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: docker
--

CREATE SEQUENCE public.notifications_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.notifications_id_seq OWNER TO docker;

--
-- TOC entry 3409 (class 0 OID 0)
-- Dependencies: 219
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: docker
--

ALTER SEQUENCE public.notifications_id_seq OWNED BY public.notifications.id;


--
-- TOC entry 218 (class 1259 OID 16399)
-- Name: routes; Type: TABLE; Schema: public; Owner: docker
--

CREATE TABLE public.routes (
    id bigint NOT NULL,
    name character varying(100) NOT NULL,
    markers bytea NOT NULL,
    author_id integer,
    counter bigint
);


ALTER TABLE public.routes OWNER TO docker;

--
-- TOC entry 217 (class 1259 OID 16398)
-- Name: routes_id_seq; Type: SEQUENCE; Schema: public; Owner: docker
--

CREATE SEQUENCE public.routes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.routes_id_seq OWNER TO docker;

--
-- TOC entry 3410 (class 0 OID 0)
-- Dependencies: 217
-- Name: routes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: docker
--

ALTER SEQUENCE public.routes_id_seq OWNED BY public.routes.id;


--
-- TOC entry 222 (class 1259 OID 16474)
-- Name: routes_users; Type: TABLE; Schema: public; Owner: docker
--

CREATE TABLE public.routes_users (
    id integer NOT NULL,
    user_id integer NOT NULL,
    route_id bigint NOT NULL
);


ALTER TABLE public.routes_users OWNER TO docker;

--
-- TOC entry 221 (class 1259 OID 16473)
-- Name: routes_users_id_seq; Type: SEQUENCE; Schema: public; Owner: docker
--

CREATE SEQUENCE public.routes_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.routes_users_id_seq OWNER TO docker;

--
-- TOC entry 3411 (class 0 OID 0)
-- Dependencies: 221
-- Name: routes_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: docker
--

ALTER SEQUENCE public.routes_users_id_seq OWNED BY public.routes_users.id;


--
-- TOC entry 216 (class 1259 OID 16390)
-- Name: users; Type: TABLE; Schema: public; Owner: docker
--

CREATE TABLE public.users (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    surname character varying(100) NOT NULL,
    email character varying(200) NOT NULL,
    password character varying(255) NOT NULL,
    token character varying(255)
);


ALTER TABLE public.users OWNER TO docker;

--
-- TOC entry 223 (class 1259 OID 16480)
-- Name: routes_users_names; Type: VIEW; Schema: public; Owner: docker
--

CREATE VIEW public.routes_users_names AS
 SELECT routes.id AS route_id,
    routes.name AS route_name,
    routes_users.user_id,
    users.name AS user_name,
    users.surname AS user_surname
   FROM ((public.routes
     JOIN public.routes_users ON ((routes.id = routes_users.route_id)))
     JOIN public.users ON ((routes_users.user_id = users.id)));


ALTER VIEW public.routes_users_names OWNER TO docker;

--
-- TOC entry 224 (class 1259 OID 16488)
-- Name: saved_routes; Type: VIEW; Schema: public; Owner: docker
--

CREATE VIEW public.saved_routes AS
 SELECT users.id AS user_id,
    routes.name AS route_name,
    routes_users.route_id
   FROM ((public.users
     JOIN public.routes_users ON ((routes_users.user_id = users.id)))
     JOIN public.routes ON ((routes.id = routes_users.route_id)));


ALTER VIEW public.saved_routes OWNER TO docker;

--
-- TOC entry 215 (class 1259 OID 16389)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: docker
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.users_id_seq OWNER TO docker;

--
-- TOC entry 3412 (class 0 OID 0)
-- Dependencies: 215
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: docker
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 3233 (class 2604 OID 16416)
-- Name: notifications id; Type: DEFAULT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.notifications ALTER COLUMN id SET DEFAULT nextval('public.notifications_id_seq'::regclass);


--
-- TOC entry 3232 (class 2604 OID 16402)
-- Name: routes id; Type: DEFAULT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.routes ALTER COLUMN id SET DEFAULT nextval('public.routes_id_seq'::regclass);


--
-- TOC entry 3234 (class 2604 OID 16477)
-- Name: routes_users id; Type: DEFAULT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.routes_users ALTER COLUMN id SET DEFAULT nextval('public.routes_users_id_seq'::regclass);


--
-- TOC entry 3231 (class 2604 OID 16393)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 3400 (class 0 OID 16413)
-- Dependencies: 220
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: docker
--

COPY public.notifications (id, user_id, message) FROM stdin;
9	4	Somebody saved your route "test1"!
\.


--
-- TOC entry 3398 (class 0 OID 16399)
-- Dependencies: 218
-- Data for Name: routes; Type: TABLE DATA; Schema: public; Owner: docker
--

COPY public.routes (id, name, markers, author_id, counter) FROM stdin;
14	p10	\\x5b7b226c6174223a35312e34393932333235393739343237372c226c6e67223a2d302e31353438333835363230313137313837387d2c7b226c6174223a35312e3531333031353930373135363735362c226c6e67223a2d302e313439353137303539333236313731397d2c7b226c6174223a35312e35313830333636393637353133312c226c6e67223a2d302e31303831343636363734383034363837367d2c7b226c6174223a35312e3530313739373235303233373833362c226c6e67223a2d302e31323033333436323532343431343036347d2c7b226c6174223a35312e3439353539393039333436373632352c226c6e67223a2d302e31313037323135383831333437363536347d2c7b226c6174223a35312e34393930313838373034303335362c226c6e67223a2d302e30383830363232383633373639353331347d5d	1	2
7	p2	\\x5b7b226c6174223a35312e34393932333235393739343237372c226c6e67223a2d302e31353438333835363230313137313837387d2c7b226c6174223a35312e3531333031353930373135363735362c226c6e67223a2d302e313439353137303539333236313731397d2c7b226c6174223a35312e35313830333636393637353133312c226c6e67223a2d302e31303831343636363734383034363837367d2c7b226c6174223a35312e3530313739373235303233373833362c226c6e67223a2d302e31323033333436323532343431343036347d2c7b226c6174223a35312e3439353539393039333436373632352c226c6e67223a2d302e31313037323135383831333437363536347d2c7b226c6174223a35312e34393930313838373034303335362c226c6e67223a2d302e30383830363232383633373639353331347d5d	1	1
8	p3	\\x5b7b226c6174223a35312e34393932333235393739343237372c226c6e67223a2d302e31353438333835363230313137313837387d2c7b226c6174223a35312e3531333031353930373135363735362c226c6e67223a2d302e313439353137303539333236313731397d2c7b226c6174223a35312e35313830333636393637353133312c226c6e67223a2d302e31303831343636363734383034363837367d2c7b226c6174223a35312e3530313739373235303233373833362c226c6e67223a2d302e31323033333436323532343431343036347d2c7b226c6174223a35312e3439353539393039333436373632352c226c6e67223a2d302e31313037323135383831333437363536347d2c7b226c6174223a35312e34393930313838373034303335362c226c6e67223a2d302e30383830363232383633373639353331347d5d	1	1
15	projekt	\\x5b7b226c6174223a35302e3035353433303438313039393333362c226c6e67223a31392e3932373339363737343239313939367d2c7b226c6174223a35302e3035393339383034303332363533352c226c6e67223a31392e39323530373933343537303331337d2c7b226c6174223a35302e30363036313032383436343937382c226c6e67223a31392e3933323337343935343232333633367d2c7b226c6174223a35302e30363232303831393632373435322c226c6e67223a31392e3933313638383330383731353832347d2c7b226c6174223a35302e3036333333373732323338313535342c226c6e67223a31392e3933323937353736393034323937327d2c7b226c6174223a35302e30353634343939353437323130362c226c6e67223a31392e3933343639323338323831323530347d5d	1	1
16	test1	\\x5b7b226c6174223a35302e3035353433303438313039393333362c226c6e67223a31392e3932373339363737343239313939367d2c7b226c6174223a35302e3035393339383034303332363533352c226c6e67223a31392e39323530373933343537303331337d2c7b226c6174223a35302e30363036313032383436343937382c226c6e67223a31392e3933323337343935343232333633367d2c7b226c6174223a35302e30363232303831393632373435322c226c6e67223a31392e3933313638383330383731353832347d2c7b226c6174223a35302e3036333333373732323338313535342c226c6e67223a31392e3933323937353736393034323937327d2c7b226c6174223a35302e30353634343939353437323130362c226c6e67223a31392e3933343639323338323831323530347d2c7b226c6174223a35302e30353637393032373637313336372c226c6e67223a31392e39343030353034333035393538387d2c7b226c6174223a35302e30363335363737393938303039362c226c6e67223a31392e39343437373131313834363230397d5d	4	1
\.


--
-- TOC entry 3402 (class 0 OID 16474)
-- Dependencies: 222
-- Data for Name: routes_users; Type: TABLE DATA; Schema: public; Owner: docker
--

COPY public.routes_users (id, user_id, route_id) FROM stdin;
2	1	7
3	1	8
9	1	14
13	2	14
14	2	7
15	2	8
16	1	15
17	4	16
\.


--
-- TOC entry 3396 (class 0 OID 16390)
-- Dependencies: 216
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: docker
--

COPY public.users (id, name, surname, email, password, token) FROM stdin;
2	Jan	Jasiński	test1@example.com	$2y$10$naOgxKOfqaWYTQ1jdVrUceLnrRp6nmFK1DKed34d3qxl0KWHTL2TS	21d1ce291527be88c1304bd4ac6edef1
3	Adam	Nowak	test123@example.com	$2y$10$v5XL0BxCdmtu7oOk08VtTusg6u4dASAgRPg6j9ON.9SqxxbH10DYK	\N
4	Adam	Ciesielski	aaa@wp.pl	$2y$10$MKZMD87Ya24ilTMIZzgeBuUNQm8l/Wlt4PHiVIRsIEjWUnhJGSTL6	\N
1	Przemysław	Nowak	p.serek@wp.pl	$2y$10$x3YQG4tmWdKGEBDImL/1FuOZ3.Lncx9rs65hbj4O.3KgqX11Y/zDK	e7477baca7d1d2f3896d4cde3e4a05a9
\.


--
-- TOC entry 3413 (class 0 OID 0)
-- Dependencies: 219
-- Name: notifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: docker
--

SELECT pg_catalog.setval('public.notifications_id_seq', 9, true);


--
-- TOC entry 3414 (class 0 OID 0)
-- Dependencies: 217
-- Name: routes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: docker
--

SELECT pg_catalog.setval('public.routes_id_seq', 16, true);


--
-- TOC entry 3415 (class 0 OID 0)
-- Dependencies: 221
-- Name: routes_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: docker
--

SELECT pg_catalog.setval('public.routes_users_id_seq', 17, true);


--
-- TOC entry 3416 (class 0 OID 0)
-- Dependencies: 215
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: docker
--

SELECT pg_catalog.setval('public.users_id_seq', 4, true);


--
-- TOC entry 3240 (class 2606 OID 16418)
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- TOC entry 3238 (class 2606 OID 16406)
-- Name: routes routes_pkey; Type: CONSTRAINT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.routes
    ADD CONSTRAINT routes_pkey PRIMARY KEY (id);


--
-- TOC entry 3242 (class 2606 OID 16479)
-- Name: routes_users routes_users_pkey; Type: CONSTRAINT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.routes_users
    ADD CONSTRAINT routes_users_pkey PRIMARY KEY (id);


--
-- TOC entry 3236 (class 2606 OID 16397)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 3246 (class 2620 OID 16485)
-- Name: routes_users decrement_route_counter_trigger; Type: TRIGGER; Schema: public; Owner: docker
--

CREATE TRIGGER decrement_route_counter_trigger AFTER DELETE ON public.routes_users FOR EACH ROW EXECUTE FUNCTION public.decrement_counter();


--
-- TOC entry 3247 (class 2620 OID 16487)
-- Name: routes_users delete_route_trigger; Type: TRIGGER; Schema: public; Owner: docker
--

CREATE TRIGGER delete_route_trigger AFTER DELETE ON public.routes_users FOR EACH ROW EXECUTE FUNCTION public.delete_route_if_no_users();


--
-- TOC entry 3248 (class 2620 OID 16486)
-- Name: routes_users increment_route_counter_trigger; Type: TRIGGER; Schema: public; Owner: docker
--

CREATE TRIGGER increment_route_counter_trigger AFTER INSERT ON public.routes_users FOR EACH ROW EXECUTE FUNCTION public.increment_counter();


--
-- TOC entry 3249 (class 2620 OID 16484)
-- Name: routes_users route_saved_notification_trigger; Type: TRIGGER; Schema: public; Owner: docker
--

CREATE TRIGGER route_saved_notification_trigger AFTER INSERT ON public.routes_users FOR EACH ROW EXECUTE FUNCTION public.notify_route_saved();


--
-- TOC entry 3244 (class 2606 OID 16497)
-- Name: routes_users routes_users_route_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.routes_users
    ADD CONSTRAINT routes_users_route_id_fkey FOREIGN KEY (route_id) REFERENCES public.routes(id) NOT VALID;


--
-- TOC entry 3245 (class 2606 OID 16492)
-- Name: routes_users routes_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.routes_users
    ADD CONSTRAINT routes_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) NOT VALID;


--
-- TOC entry 3243 (class 2606 OID 16419)
-- Name: notifications user; Type: FK CONSTRAINT; Schema: public; Owner: docker
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES public.users(id);


-- Completed on 2024-06-16 06:24:43 UTC

--
-- PostgreSQL database dump complete
--

