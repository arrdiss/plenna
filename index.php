<?php

require 'Routing.php';


$path = parse_url(trim($_SERVER['REQUEST_URI'], '/'), PHP_URL_PATH);

Routing::get('login', 'DefaultController');
Routing::get('register', 'DefaultController');
Routing::get('dashboard', 'DefaultController');
Routing::get('create_route', 'DefaultController');
Routing::get('search_routes', 'DefaultController');
Routing::get('analysis', 'DefaultController');
Routing::get('notifications', 'DefaultController');
Routing::get(null, 'DefaultController');
Routing::post('signin', 'SecurityController');
Routing::post('signup', 'SecurityController');
Routing::get('signout', 'SecurityController');
Routing::post('save_route', 'RoutesController');
Routing::post('search_routes', 'RoutesController');
Routing::post('save_user_route', 'RoutesController');
Routing::post('delete_route', 'RoutesController');
Routing::post('show_route', 'RoutesController');
Routing::get('get_route_stats', 'RoutesController');
Routing::post('dashboard', 'DefaultController');
Routing::post('delete_notification', 'NotificationController');
Routing::post('delete_all_notifications', 'NotificationController');

Routing::run($path);