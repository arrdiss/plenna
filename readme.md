# Plenna
- [Plenna](#plenna)
  - [Cel](#cel)
  - [Funkcjonalność](#funkcjonalność)
    - [Logowanie](#logowanie)
    - [Tworzenie tras](#tworzenie-tras)
    - [Wyszukiwanie tras](#wyszukiwanie-tras)
    - [Analizy popularności](#analizy-popularności)
    - [Powiadomienia](#powiadomienia)
  - [Technologie](#technologie)
    - [PHP](#php)
    - [HTML/CSS/JS](#htmlcssjs)
    - [PostgreSQL](#postgresql)
    - [nginx](#nginx)
    - [PGAdmin](#pgadmin)
    - [Docker](#docker)
  - [Struktura bazy danych](#struktura-bazy-danych)
  - [Instalacja](#instalacja)
  - [Demo](#demo)
  - [Licencja](#licencja)

## Cel
Celem projektu było stworzenie prostej platformy, umożliwiającej użytkownikom tworzenie tras na mapach ( na przykład tras górskich czy rowerowych) i dzielenie się nimi z inymi. To proste!

## Funkcjonalność
### Logowanie
Podstawową funkcją Plenny jest tworzenie użytkowników, możliwość logowania się, które jest realizowane na bazie tokenu, który przechowywany jest w sesji.
![dashboard](images/Screenshot%202024-06-15%20at%2015.14.33.png)
Już na pulpicie widzimy nasze dane oraz zapisane trasy. Możemy je usunąć, wyświetlić, lub zmienić swoje dane w serwisie.
### Tworzenie tras
Plenna umożliwia tworzenie tras na bazie otwartych map OpenSteetMaps przy wykorzystaniu biblioteki Leajlet.js. Za pomocą znaczników edytujemy trasę aby nastepnie móc ją zapisać i udostępnić innym.
![create_route](images/Screenshot%202024-06-15%20at%2015.15.27.png)
### Wyszukiwanie tras
Użytkownicy mają również możliwość wyszukiwania tras innych użytkowników. Wystarczy wpisać nazwę trasy w pole wyszukiwania, a następnie wyświetlić lub od razu zapisać interesującą nas trasę.
 ![search_routes](images/Screenshot%202024-06-15%20at%2015.15.37.png)
### Analizy popularności
W serwisie dostępny jest również sposób na sprawdzanie popularności swoich tras. Na podstawie zapisów lub usunięcia trasy przez innych użytkowników aplikacja jest w stanie stwierdzić zapytań ma aktualnie nasza trasa.
![analysis](images/Screenshot%202024-06-15%20at%2015.15.40.png)
### Powiadomienia
Plenna wysyła użytkownikowi powiadomienie o dodaniu jego trasy do ulubionych. Powiadomienia są widoczne na dedykowanej podstronie, gdzie mna je usunąć.
![notifications](images/Screenshot%202024-06-15%20at%2015.17.51.png)

## Technologie
### PHP 
Bazą całego projektu jest język PHP używany do wyświetlania stron, manipulacji ich treścią oraz do wysyłania zapytań do bazy danych.
### HTML/CSS/JS
HTML, CSS oraz JavaScript zostały użyte od strony klienta. Odpowiadają za wygląd i funkcjonowanie projektu.
### PostgreSQL
Silnikiem bazy danych jest PostgreSQL.
### nginx
nginx jest popularnym i otwartoźródłowym serwerem, wysoce skalowalnym, o dużych możliwościach konfiguracji.
### PGAdmin
Do obsługi bazy danych PGSQL wykorzystałem środowisko PGAdmin.
### Docker
Całośc zamknięta została w kontenerach Dockera, aby ułatwić pracę z projektem oraz umożliwić łątwe przenoszenie całości.

## Struktura bazy danych
Struktura bazy danych została przedstawiona na poniższym diagramie:
![diagram](images/Screenshot%202024-06-15%20at%2016.05.57.png)
W bazie danych zastosowano również wyzwalacze, funkcje oraz widoki.

## Instalacja
W celu instalacji należy sklonować poniższe repozytorium, mieć zainstalowany silnik Dockera a nastepnie wywołać polecenie *docker-compose build*. Po przejściu na stronę [Logowania](http://localhost:8080) będziemy mogli zacząć korzystać z serwisu. Natomiast w celu połączenia się z PGAdmin należy użyć [tego linku](http://localhost:5050).
![pgadmin](images/Screenshot%202024-06-15%20at%2016.10.09.png)

## Demo
W powyższych punktach opisałem funkcjonalność i działanie aplikacji wraz z dołączonymi zrzutami ekranu.

## Licencja
Program jest wolny do użytku dla każdego kto ma ochote sprawdzić jego działanie. Rpzwiązanie można dowolnie modyfikować i zmieniać bez zgody autora. Nie wymagane jest wyszczególnianie oryinalnego autora projektu.