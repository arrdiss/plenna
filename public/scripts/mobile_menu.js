const hamburgerBtn = document.getElementById('hamburger-btn');
const mobileNav = document.getElementById('mobile-nav');
const content = document.getElementsByClassName("content")[0];

hamburgerBtn.addEventListener('click', () => {
    if (mobileNav.style.display === "block") {
        mobileNav.style.display = "none";
        content.style.zIndex = 0;
      } else {
        mobileNav.style.display = "block";
        content.style.zIndex = -1;
      }
});
