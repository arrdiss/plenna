// Inicjalizacja mapy
var map = L.map('map',
    {
    contextmenu: true,
    contextmenuWidth: 140,
    contextmenuItems: [
      {
        text: 'Reset Route',
        callback: resetRoute
      },
      {
        text: 'Remove Last Marker',
        callback: removeLastMarker
      },
      {
        text: 'Save Route',
        callback: saveRoute
      }
    ]
  }).setView([51.505, -0.09], 13);

// Dodanie warstwy kafelkowej z OSM
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

var geocoder = L.Control.geocoder({
    defaultMarkGeocode: false
  })
  .on('markgeocode', function(e) {
    var bbox = e.geocode.bbox;
    var poly = L.polygon([
      [bbox.getSouthEast().lat, bbox.getSouthEast().lng],
      [bbox.getNorthEast().lat, bbox.getNorthEast().lng],
      [bbox.getNorthWest().lat, bbox.getNorthWest().lng],
      [bbox.getSouthWest().lat, bbox.getSouthWest().lng]
    ]).addTo(map);
    
    map.fitBounds(poly.getBounds());

    // Usuń prostokąt po dopasowaniu widoku mapy
    setTimeout(function() {
      map.removeLayer(poly);
    }, 100);
  })
  .addTo(map);

// Tablica do przechowywania znaczników
var markers = [];
var route;

// Obsługa zdarzenia kliknięcia na mapie
map.on('click', function(e) {
    var marker = L.marker(e.latlng).addTo(map);
    marker.on('click', (m) => {
        const index = markers.indexOf(m.target);
        if (index > -1) { // only splice array when item is found
            map.removeLayer(markers[index]);
            markers.splice(index, 1); // 2nd parameter means remove one item only
        }
        drawRoute();
    });
    markers.push(marker);
    drawRoute();
});

var urlParams = new URLSearchParams(window.location.search);
if (urlParams.has('route_id')) {
    var routeData = decodeURIComponent(urlParams.get('route_id'));
    loadRoute(parseInt(routeData));
}

// Funkcja do rysowania linii między znacznikami
function drawRoute() {
    var latlngs = markers.map(function(marker) {
        return marker.getLatLng();
    });

    if (latlngs.length > 1) {
        // Usuwanie poprzedniej trasy, jeśli istnieje
        if (typeof route !== 'undefined') {
            map.removeLayer(route);
        }
        // Rysowanie nowej trasy
        route = L.polyline(latlngs, {color: 'blue'}).addTo(map);
    }
}

// Funkcja do resetowania trasy
function resetRoute() {
    markers.forEach(function(marker) {
        map.removeLayer(marker);
    });
    markers = [];
    if (typeof route !== 'undefined') {
        map.removeLayer(route);
    }
}

// Funkcja do usuwania ostatniego znacznika
function removeLastMarker() {
    var lastMarker = markers.pop();
    if (lastMarker) {
        map.removeLayer(lastMarker);
        drawRoute();
    }
}

// Funkcja do zapisywania trasy
function saveRoute() {

    if (markers.length == 0) {
        alert("Create route!");
        return;
    }

    var routeData = markers.map(function(marker) {
        return marker.getLatLng();
    });

    // Zapytanie użytkownika o nazwę trasy
    var routeName = prompt("Route name:");

    // Sprawdzenie, czy użytkownik podał nazwę
    if (routeName === null || routeName.trim() === "") {
        alert("Name cannot be empty.");
        return;
    }

    var data = {
        name: routeName,
        route: routeData
    };

    var jsonRouteData = JSON.stringify(data);

    // Wysłanie danych do serwera
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "save_route", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                console.log(xhr.responseText);
                var response = JSON.parse(xhr.responseText);
                if (response.status === "success") {
                    alert("Route saved successfully! " + response.message);
                } else {
                    alert("Error: " + response.message);
                }
            } else {
                alert("Request failed. Returned status of " + xhr.status);
            }
        }
    };
    xhr.send(jsonRouteData);
}

function loadRoute(route_id) {
    // Wysłanie danych do serwera
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "show_route", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var response = JSON.parse(xhr.responseText);
            if (response.status === "success") {
                let _markers = JSON.parse(response.markers);
                _markers.forEach(function(point) {
                    var marker = L.marker([point.lat, point.lng]).addTo(map);
                    marker.on('click', (m) => {
                        const index = markers.indexOf(m.target);
                        if (index > -1) { // only splice array when item is found
                            map.removeLayer(markers[index]);
                            markers.splice(index, 1); // 2nd parameter means remove one item only
                        }
                        drawRoute();
                    });
                    markers.push(marker);
                });
                drawRoute();
            } else {
                alert("Error: " + response.message);
            }
        }
    };
    xhr.send(JSON.stringify({ id: route_id}));
}

document.getElementById('save').addEventListener('click', saveRoute);