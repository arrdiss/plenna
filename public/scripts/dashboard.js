let buttons = document.getElementsByClassName('delete');
console.log(buttons.length);

for (let btn of buttons) {
    btn.addEventListener('click', delete_route);
}

buttons = document.getElementsByClassName('show_route');

for (let btn of buttons) {
    btn.addEventListener('click', show_route);
}

function delete_route(e) {
    console.log('click');
    let route_id = parseInt(e.target.parentElement.getAttribute('data-route-id'));

    // Wysłanie danych do serwera
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "delete_route", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            console.log(xhr.responseText);
            var response = JSON.parse(xhr.responseText);
            if (response.status === "success") {
                alert(response.message);
                location.reload();
            } else {
                alert("Error: " + response.message);
            }
        }
    };
    xhr.send(JSON.stringify({ id: route_id}));
}

function show_route(e) {
    console.log('click');
    let route_id = parseInt(e.target.parentElement.getAttribute('data-route-id'));
    window.location = "create_route?route_id=" + route_id;
}