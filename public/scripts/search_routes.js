function searchRoutes() {
    var routeName = document.getElementById('route-name').value;

    // Wysłanie danych do serwera
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "search_routes", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            console.log(xhr.responseText);
            var response = JSON.parse(xhr.responseText);
            if (response.status === "success") {
                displayResults(response.routes);
            } else {
                alert("Error: " + response.message);
            }
        }
    };
    xhr.send(JSON.stringify({ name: routeName }));
}

function showRoute(e) {
    console.log('click');
    let route_id = parseInt(e.target.parentElement.getAttribute('data-id'));
    window.location = "create_route?route_id=" + route_id;
}

function displayResults(routes) {
    var resultsDiv = document.getElementById('results');
    resultsDiv.innerHTML = '';

    if (routes.length > 0) {
        // var ul = document.createElement('ul');
        routes.forEach(function(route) {
            var div = document.createElement('div');
            div.textContent = "Route Name: " + route.name;
            div.classList.add("result");
            div.setAttribute('data-id', route.id);

            var button = document.createElement('button');
            button.onclick = saveRoute;
            button.innerText = "Save";
            div.appendChild(button);

            button = document.createElement('button');
            button.onclick = showRoute;
            button.innerText = "Show route";
            div.appendChild(button);

            resultsDiv.appendChild(div);
        });
    } else {
        resultsDiv.textContent = 'No routes found.';
    }
}

function saveRoute(e) {
    let route_id = parseInt(e.target.parentElement.getAttribute('data-id'));
    
    // Wysłanie danych do serwera
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "save_user_route", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            console.log(xhr.responseText);
            var response = JSON.parse(xhr.responseText);
            if (response.status === "success") {
                alert(response.message);
            } else {
                alert("Error: " + response.message);
            }
        }
    };
    xhr.send(JSON.stringify({ id: route_id}));
}