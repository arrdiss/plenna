function fetchRouteStats() {
    fetch('get_route_stats')
        .then(response => response.json())
        .then(data => {
            const statsDiv = document.getElementById('route-stats');
            statsDiv.innerHTML = '';

            if (data.length === 0) {
                statsDiv.innerHTML = '<p>No routes</p>';
            } else {
                console.log(data);
                data.forEach(route => {
                    const routeDiv = document.createElement('div');
                    routeDiv.innerHTML = `<p>Name: ${route.name} - counter: ${route.counter}</p>`;
                    statsDiv.appendChild(routeDiv);
                });
            }
        })
        .catch(error => {
            console.error('Error:', error);
        });
}

window.onload = fetchRouteStats;