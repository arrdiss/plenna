function deleteAllNotifications(userId) {
    if (confirm('Delete all notifications?')) {
        // Wywołaj zapytanie AJAX do PHP, aby usunąć wszystkie powiadomienia
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'delete_all_notifications');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            if (xhr.status === 200) {
                // Odśwież stronę lub wykonaj inne działania po usunięciu wszystkich powiadomień
                location.reload();
            } else {
                alert('Cannot delete notifications.');
            }
        };
        xhr.send(JSON.stringify({user_id: userId}));
    }
}

function deleteNotification(notificationId) {
    // Wywołaj zapytanie AJAX do PHP, aby usunąć powiadomienie
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'delete_notification');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (xhr.status === 200) {
            // Odśwież stronę lub wykonaj inne działania po usunięciu powiadomienia
            location.reload();
        } else {
            alert('Cannot delete notification');
        }
    };
    console.log(notificationId);
    xhr.send(JSON.stringify({notification_id: notificationId}));
}