<?php
    // Połączenie z bazą danych
    try {
        // Pobranie danych użytkownika
        $stmt = PDO_singleton::getInstance()->prepare("SELECT * FROM users WHERE id = :id");
        $stmt->bindParam(':id', $_SESSION['user_id'], PDO::PARAM_INT);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
    
        // Pobranie tras użytkownika
        $stmt = PDO_singleton::getInstance()->prepare("SELECT * FROM saved_routes WHERE user_id = :user_id");
        $stmt->bindParam(':user_id', $_SESSION['user_id'], PDO::PARAM_INT);
        $stmt->execute();
        $routes = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    } catch (PDOException $e) {
        echo "Database error: " . $e->getMessage();
        exit();
    }
    
    // Aktualizacja danych użytkownika
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_user'])) {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $password = !empty($_POST['password']) ? password_hash($_POST['password'], PASSWORD_BCRYPT) : $user['password'];
    
        try {
            $stmt = PDO_singleton::getInstance()->prepare("UPDATE users SET name = :first_name, surname = :last_name, email = :email, password = :password WHERE id = :id");
            $stmt->bindParam(':first_name', $first_name, PDO::PARAM_STR);
            $stmt->bindParam(':last_name', $last_name, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':password', $password, PDO::PARAM_STR);
            $stmt->bindParam(':id', $_SESSION['user_id'], PDO::PARAM_INT);
            $stmt->execute();
    
            // Odświeżenie danych użytkownika po aktualizacji
            $stmt = PDO_singleton::getInstance()->prepare("SELECT * FROM users WHERE id = :id");
            $stmt->bindParam(':id', $_SESSION['user_id'], PDO::PARAM_INT);
            $stmt->execute();
            $user = $stmt->fetch(PDO::FETCH_ASSOC);

            $_SESSION["username"] = $_POST['first_name']." ".$_POST['last_name'];
            $_SESSION["email"] = $_POST['email'];
    
            $message = "User data updated successfully!";
        } catch (PDOException $e) {
            $message = "Error updating user data: " . $e->getMessage();
        }
    }
?>
<h1>User Dashboard</h1>

<?php if (isset($message)): ?>
    <p><?= htmlspecialchars($message) ?></p>
<?php endif; ?>

<h2>User Information</h2>
<form action="dashboard" method="POST">
    <input type="hidden" name="update_user" value="1">
    <label for="first_name">First Name:</label>
    <input type="text" id="first_name" name="first_name" value="<?= htmlspecialchars($user['name']) ?>" required>
    <br>
    <label for="last_name">Last Name:</label>
    <input type="text" id="last_name" name="last_name" value="<?= htmlspecialchars($user['surname']) ?>" required>
    <br>
    <label for="email">Email:</label>
    <input type="email" id="email" name="email" value="<?= htmlspecialchars($user['email']) ?>" required>
    <br>
    <label for="password">Password (leave blank to keep current password):</label>
    <input type="password" id="password" name="password">
    <br>
    <button type="submit">Update</button>
</form>

<h2>Your Routes</h2>
<ul>
    <?php foreach ($routes as $route): ?>
        <li>
            <div data-route-id="<?= htmlspecialchars($route['route_id']) ?>">
                <strong>Name:</strong> <?= htmlspecialchars($route['route_name']) ?><button class="delete">Delete</button><button class="show_route">Show route</button>
            </div>
        </li>
    <?php endforeach; ?>
</ul>

<script src="public/scripts/dashboard.js"></script>