<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/content_style.css">
    <!-- Link to Bootstrap Icons CDN -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.8.1/font/bootstrap-icons.min.css">
    <title>Menu</title>
</head>
<body>
    <div class="sidebar">
        <div class="logo">
            <img src="public/img/logo.svg" alt="logo">
        </div>
        <div class="account-info">
            <img src="public/img/avatar.svg" alt="avatar">
            <div class="user-info">
                <p><?php echo htmlspecialchars($_SESSION["username"], ENT_QUOTES, 'UTF-8'); ?></p>
                <p><?php echo htmlspecialchars($_SESSION["email"], ENT_QUOTES, 'UTF-8'); ?></p>
            </div>
        </div>
        <nav>
            <a href="dashboard"><i class="bi bi-house icon-orange"></i> Dashboard</a>
            <a href="create_route"><i class="bi bi-signpost-2 icon-orange"></i> Create route</a>
            <a href="search_routes"><i class="bi bi-search icon-orange"></i> Search routes</a>
            <a href="analysis"><i class="bi bi-bar-chart icon-orange"></i> Analysis</a>
            <a href="notifications"><i class="bi bi-bell icon-orange"></i> Notifications</a>
            <a href="signout"><i class="bi bi-box-arrow-left icon-orange"></i> Logout</a>
        </nav>
    </div>
    
    <div class="mobile-menu">
        <div class="topbar">
            <div class="logo">
                <img src="public/img/logo.svg" alt="logo">
            </div>
            <div class="hamburger">
                <button id="hamburger-btn" aria-label="Menu Toggle">
                    <i class="bi bi-list"></i>
                </button>
            </div>
        </div>
        <nav id="mobile-nav">
            <a href="dashboard"><i class="bi bi-house icon-orange"></i> Dashboard</a>
            <a href="create_route"><i class="bi bi-signpost-2 icon-orange"></i> Create route</a>
            <a href="search_routes"><i class="bi bi-search icon-orange"></i> Search routes</a>
            <a href="analysis"><i class="bi bi-bar-chart icon-orange"></i> Analysis</a>
            <a href="notifications"><i class="bi bi-bell icon-orange"></i> Notifications</a>
            <a href="signout"><i class="bi bi-box-arrow-left icon-orange"></i> Logout</a>
        </nav>
    </div>
    
    <div class="content">
        <?php include $content_file; ?>
    </div>
    
    <script src="public/scripts/mobile_menu.js"></script>
</body>
</html>
