<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/css/login.css">
    <title>Login to Plenna</title>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg" alt="logo">
        </div>
        <div class="login-container">
            <p>Sign in</p>
            <form action="signin" method="post">
                <label for="email">Your email</label>
                <input type="email" name="email" id="email">
                <label for="password">Your password</label>
                <input type="password" name="password" id="password">
                <button class="button">Login</button>
            </form>
        </div>
        <p>New to our community</p>
        <a href="register" class="button white-button"><span>Create an account</span></a>
    </div>
</body>
</html>