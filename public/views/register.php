<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><link rel="stylesheet" href="public/css/reset.css">
    <link rel="stylesheet" href="public/css/login.css">
    <title>Register in Plenna</title>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg" alt="logo">
        </div>
        <div class="login-container">
            <p>Sign up</p>
            <form action="signup" method="post">
                <label for="email">Email: </label>
                <input type="email" name="email" id="email">
                <label for="password">Password: </label>
                <input type="password" name="password" id="password">
                <label for="name">Name: </label>
                <input type="text" name="name" id="name">
                <label for="surname">Surname: </label>
                <input type="text" name="surname" id="surname">
                <button type="submit" class="button">Register</button>
            </form>
        </div>
    </div>
</body>
</html>