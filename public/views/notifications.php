<h1>Notifications</h1>
<?php
try {
    // Pobierz powiadomienia dla danego użytkownika
    $user_id = $_SESSION['user_id']; // Przykładowe pobranie user_id z parametrów URL
    $sql = "SELECT id, message FROM notifications WHERE user_id = :user_id";
    $stmt = PDO_singleton::getInstance()->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->execute();
    $notifications = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo '<div class="notifications">';
    // Wyświetl powiadomienia
    if ($notifications) {
        foreach ($notifications as $notification) {
            echo '<div>';
            echo '<span>' . htmlspecialchars($notification['message']) . '</span>';
            echo '<button onclick="deleteNotification(' . $notification['id'] . ')">Delete</button>';
            echo '</div>';
        }
    } else {
        echo '<p>No notifications!</p>';
    }
    echo '</div>';

} catch (PDOException $e) {
    echo "Data base error: " . $e->getMessage();
}
?>
<div class="button-container">
    <button id="delete_all" onclick="deleteAllNotifications(<?php echo $user_id; ?>)">Delete all</button>
</div>

<script src="public/scripts/notifications.js"></script>