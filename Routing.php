<?php

require_once "src/repository/config.php";
require_once "src/controllers/DefaultController.php";
require_once "src/controllers/SecurityController.php";
require_once "src/controllers/RoutesController.php";
require_once "src/controllers/NotificationController.php";

class Routing {

  public static $routes;
  public static $post_routes;

  public static function get($url, $view) {
    self::$routes[$url] = $view;
  }

  public static function post($url, $view) {
    self::$post_routes[$url] = $view;
  }

  public static function run ($url) {
    $action = explode("/", $url)[0];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      if (!array_key_exists($action, self::$post_routes)) {
        die("Wrong url!");
      }

      $controller = self::$post_routes[$action];
    } else {
      if (!array_key_exists($action, self::$routes)) {
        die("Wrong url!");
      }

      $controller = self::$routes[$action];
    }

    $object = new $controller;
    $action = $action ?: 'dashboard';

    if ($action == 'login' || $action == 'register' || $action == 'signin' || $action == 'signup')
      $object->$action();
    else if (Routing::validate_token())
      $object->$action();
    else 
      header("Location: login");

  }

  private static function validate_token() : bool {
    session_start();
    if (!isset($_SESSION['user_id']) || !isset($_SESSION['token'])) {
        return false;
    }
    
    $stmt = PDO_singleton::getInstance()->prepare("SELECT token FROM users WHERE id = ?");
    $stmt->execute([$_SESSION['user_id']]);
    $user = $stmt->fetch();
    
    if ($user && $user['token'] === $_SESSION['token']) {
        return true;
    } else {
        return false;
    }
  }
}